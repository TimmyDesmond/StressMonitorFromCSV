package com.example.tydes.stressmonitorfromcsvstream;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;
import android.widget.SimpleCursorTreeAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;


public class Background_Data_Stream_Service extends IntentService {

    private static final String TAG = "Background_Service";
    private List<Float> ibiSample;
    private List<Float> edaSample;

    public Background_Data_Stream_Service() {
        super("Background_Data_Stream_Service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {



        Log.v(TAG, "Message will be show in 10 Seconds");
        if (intent !=null){
            synchronized (this){
                for (int i = 0; i<2; i++){
                    try {
                        wait(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                readCSV();
                Log.v(TAG, "10 Seconds has passed");
            }
        }
    }

    private List<CSVdata> bvpSamples = new ArrayList<>();

    private void readCSV() {
        ibiSample = new ArrayList<Float>();
        edaSample = new ArrayList<Float>();

        InputStream bvpCsv = getResources().openRawResource(R.raw.bvp);
        BufferedReader readerBvp = new BufferedReader(
                new InputStreamReader(bvpCsv, Charset.forName("UTF-8"))
        );
        InputStream edaCsv = getResources().openRawResource(R.raw.eda);
        BufferedReader readerEda = new BufferedReader(
                new InputStreamReader(edaCsv, Charset.forName("UTF-8"))
        );
        InputStream ibiCsv = getResources().openRawResource(R.raw.ibi);
        BufferedReader readerIbi = new BufferedReader(
                new InputStreamReader(ibiCsv, Charset.forName("UTF-8"))
        );


        String lineBvp= "";
        String lineEda= "";
        String lineIbi= "";
        try {
            readerBvp.readLine();
            readerEda.readLine();
            readerBvp.readLine();
            readerEda.readLine();
            readerIbi.readLine();
            CSVdata ibiData = new CSVdata();
            int count = 0;
            float countForTimeCol = 0;
            float curTime;
            boolean getIBI  = true;
            boolean timeGreaterThan60 = false;

            while (((lineBvp = readerBvp.readLine()) != null)) {

                wait(16);
                count += 1;
                countForTimeCol += 1;
                curTime = (((float) 1) / ((float) 64)) * countForTimeCol;
                if (curTime > 60){
                    timeGreaterThan60 = true;
                }
                String[] tokensBvp = lineBvp.split(",");
                CSVdata bvpData = new CSVdata();
                bvpData.setCol_1(Float.parseFloat(tokensBvp[0]));



                if (getIBI) {
                    if ((lineIbi = readerIbi.readLine()) != null) {
                        String[] tokensIbi = lineIbi.split(",");
                        ibiData.setCol_1(Float.parseFloat(tokensIbi[0]));
                        ibiData.setCol_2(Float.parseFloat(tokensIbi[1]));
                        getIBI = false;
                    }
                }

                if (ibiData.getCol_1() != 0){
                    if (ibiData.getCol_1()<=curTime){
                        Log.d("Reader", "IBI: " + ibiData.getCol_1() + " At time: " + ibiData.getCol_2());
                        getIBI = true;
                    }
                }
                Log.d("Reader", "BVP: " + bvpData.getCol_1() + " At time: " + curTime);
                if (count == 16 && ((lineEda = readerEda.readLine()) != null)) {
                    String[] tokensEda = lineEda.split(",");
                    CSVdata edaData = new CSVdata();
                    edaData.setCol_1(Float.parseFloat(tokensEda[0]));
                    count = 0;
                    Log.d("Reader", "EDA: " + edaData.getCol_1() + " At time: " + curTime);
                    edaSample.add(edaData.getCol_1());
                    if (timeGreaterThan60){
                        edaSample.remove(0);
                        Log.d("Reader", "Time Greater tha 60 seconds");
                    }
                }


            }
        } catch (IOException e) {
            Log.wtf("CSV READER", "Error Reding Data on line" + lineBvp, e);
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
