package com.example.tydes.stressmonitorfromcsvstream;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class HomePageActivity extends AppCompatActivity {

    private Button connectButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        connectButton = (Button) findViewById(R.id.connectButton);

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Connecting to Device....",
                        Toast.LENGTH_LONG ).show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Intent serviceIntent = new Intent(HomePageActivity.this,
                                Background_Data_Stream_Service.class);
                        startService(serviceIntent);
                        Intent intent = new Intent(HomePageActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }, 2500);   //5 seconds
            }
        });
    }
}
